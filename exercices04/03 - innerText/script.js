function roll(faces =6) {
  return Math.ceil(Math.random() * faces);
}
function rollAll() {
    let roll1,roll2,total;
    roll1 = roll();
    roll2 = roll();
    total = +roll1+roll2;

    document.querySelector("#dice1").innerText = roll1;
    document.querySelector("#dice2").innerText = roll2;
    document.querySelector("#total").innerText = total;
}
