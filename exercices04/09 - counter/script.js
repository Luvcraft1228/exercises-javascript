var max = document.querySelector("#max").value;
const countDisplay = document.querySelector("#count");
var full = document.querySelector("#full").style;
var count = +countDisplay.innerHTML;

function changeMax() {
 
    max = document.querySelector("#max").value;
    if(count > max) decrement();
}

function decrement() {
 if(count >0){
   full.display ="none";
   count--;
   countDisplay.innerHTML = count;
 }
}

function increment() {
  if(count<max){
    count++;
    countDisplay.innerHTML = count;
  } else full.display = "";
}
