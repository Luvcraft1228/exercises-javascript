/* Voici un exemple de la structure à ajouter à root
<tr>
  <th scope="col">1</th>
  <td>Balayeuse</td>
  <td>Passer la balayeuse demain</td>
  <td></td>
</tr>
*/
const root = document.querySelector("#root");
let count =0;

function add(){
  count++;

  let index = document.createElement("th");
  index.innerHTML = count;
  
  
  let newLine = document.createElement("tr");
  newLine.appendChild(index);
  
  let values = document.querySelectorAll("tfoot tr td input");
  values.forEach((inputField)=>{
    let newValue = document.createElement("td");
    newValue.innerHTML = inputField.value;
    newLine.appendChild(newValue);
    inputField.value = "";
  })
  newLine.appendChild(createBtn());
  root.appendChild(newLine);
}

function deleteLine(){
  this.parentElement.parentElement.remove();
}

function createBtn(){
  let deleteBtn = document.createElement("button");
  deleteBtn.innerHTML = "Supprimer";
  deleteBtn.classList.add("btn");
  deleteBtn.classList.add("btn-danger");
  deleteBtn.onclick = deleteLine;
  let deleteBtnTd = document.createElement("td");
  deleteBtnTd.appendChild(deleteBtn);
  return deleteBtnTd;
}


