

const dateInput = document.getElementsByTagName("input")[0];
const ageList = document.createElement("ul");
document.getElementsByTagName("div")[0].appendChild(ageList);

dateInput.addEventListener( "change", printInfo);

function printInfo(){
    let newLi = document.createElement("li");
    let requestNbr = ageList.childNodes.length+1;
    newLi.innerHTML = `Requête #${requestNbr} Age: ${checkAge()} ans`;
    ageList.appendChild(newLi);
}

function checkAge(){
   let today = new Date();
   let birthday = new Date(dateInput.value);
   let age = today.getFullYear()-birthday.getFullYear();
   let month = today.getMonth() -  birthday.getMonth();
   if(month < 0 || (month ===0 && today.getDate()< birthday.getDate())) age --;
  
   return age; 
   
}

