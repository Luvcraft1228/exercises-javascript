
const inputs = document.querySelectorAll("input");
const selectBox = document.querySelector("select");
const nbr1 = inputs[0];
const nbr2 = inputs[1];
const result = inputs[2];
result.readOnly = true;

inputs.forEach((input) => input.addEventListener("keyup", errorCheck));
inputs.forEach((input) => input.addEventListener("keyup", calculate));
selectBox.addEventListener("change", calculate);

function calculate(){
    let mathString = nbr1.value + selectBox.value + nbr2.value;
    result.value = eval(mathString);
}
function errorCheck(){
   if (isNaN(this.value)){
       this.style.border = "red 2px solid";
       result.value = "";
   }else {
    this.style.border = "";
   }
}


